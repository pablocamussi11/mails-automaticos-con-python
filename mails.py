import smtplib
import ssl


def mandar_mail(mail_destino, nombre_de_persona):
    mail_origen = "pablocamussi11@gmail.com"  # Mail origen
    mail_destino = mail_destino  # Mail destino (ingresado por parámetro, viene del diccionario)
    contraseña = #insertar contraseña

    subject = #Asunto
    text = nombre_de_persona + #mensaje

    mensaje = 'Subject: {}\n\n{}'.format(subject, text)

    context = ssl.create_default_context()
    with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
        server.login(mail_origen, contraseña)
        server.sendmail(mail_origen, mail_destino, mensaje.encode("utf-8"))
        print("Enviando mail a " + mail_destino)


mails = {
    "Pablo Camussi":"pablo.camussi@hotmail.com" #Insertar lista de mails en diccionario (Nombre : mail)
}

for nombre in mails:
    mandar_mail(mails[nombre], nombre)

print("FINALIZADO")
